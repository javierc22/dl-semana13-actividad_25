class SalesController < ApplicationController
  def new
    @sale = Sale.new
  end

  def create
    @sale = Sale.new(sale_params)
    # Aplicando descuento al total:
    @sale.total = @sale.value - (@sale.value * @sale.discount / 100)

    # Aplicando Iva:
    if @sale.tax.zero?
      @sale.total =  @sale.total * 1.19
      @sale.tax = 19
    else
      @sale.tax = 0
    end

    # Respuesta:
    respond_to do |format|
      if @sale.save
        format.html { redirect_to sales_done_path, notice: 'El registro fue almacenado con éxito!' }
      else
        format.html { render :new }
      end
    end
  end

  def done
    @sales = Sale.all
  end

  private
  def sale_params
    params.require(:sale).permit(:cod, :detail, :category, :value, :discount, :tax, :total)
  end
end
