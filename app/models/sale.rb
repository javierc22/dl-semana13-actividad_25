class Sale < ApplicationRecord
  # El campo cod debe ser único (uniqueness)
  validates :cod, uniqueness: true
  # El detalle debe estar presente (presence)
  validates :detail, presence: true
  # La categoría debe estar contenida dentro de la siguiente colección: (inclusion)
  validates :category, inclusion: %w('uno' 'dos' 'tres' 'cuatro')
  # El campo value debe ser entero y positivo (numericality)
  validates :value, numericality: true
  # El campo discount debe ser entero, positivo y menor o igual a 40
  validates :discount, numericality: { only_integer: true, less_than_or_equal_to: 40 }
end
